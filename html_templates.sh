#!/bin/bash
###################################################################### 
#Copyright (C) 2020  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

tags="<h1></h1>
<img src='' id='' class='' />
<div></div>
<div class=''></div>
<div id='' class=''></div>
<div id=''></div>
<!DOCTYPE html>
<html lang='en'></html>
<head></head>
<title></title>
<meta charset='utf-8'>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<style></style>
<script></script>
<body></body>
<div class='flex'></div>
<input type='text' />
<input type='submit' value='submit' />
<button class='button'></button>
<form method='post' action=''></form>
<select id='' name=''></select>
<select id='' class='' name=''></select>
<select class='' name=''></select>
<select name=''></select>
"
echo "$tags" |fzf-tmux
