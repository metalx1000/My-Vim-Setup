#!/bin/bash


cp -v vimrc ~/.vimrc
cp -v tmux.conf ~/.tmux.conf
mkdir -p ~/.tmux/
cp -rv tmux/* ~/.tmux/
cp -v vim_templates.sh ~/.vim/
cp -v html_templates.sh ~/.vim/
cp -v scripts_search.sh ~/.vim/scripts_search.sh

mkdir -p ~/.vim/skeleton
cp -vr skeleton/* ~/.vim/skeleton

mkdir -p ~/.vim/scripts
cp -vr scripts/* ~/.vim/scripts

cp tern-config ~/.tern-config

sed -i 's/\/home\/metalx1000/$HOME/g' ~/.vimrc;grep "^iab" ~/.vimrc|awk '{print $2}'|sort > ~/.vim/skeleton/dic.lst

if [ "$1" = "full" ];then
  clear
  echo "==============Performing full install=========="
  sleep 3
  #sudo apt install vim-nox build-essential cmake node vim-addon-manager vim-youcompleteme
  sudo apt install vim-nox vim-addon-manager fzf git tmux 
  #cd $HOME/.vim/bundle/YouCompleteMe/
  #./install.py --tern-completer
  #vam install youcompleteme
  sudo apt install vim-youcompleteme
  vim-addon-manager install youcompleteme
  
  git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
  git clone https://github.com/mattn/emmet-vim.git ~/.vim/bundle/emmet-vim
  #install plugin manager for vim
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

  #install fzf
  git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
  ~/.fzf/install


  vim +PlugInstall
  vim +PluginInstall +qall
fi


echo "===============Great Tern and Youcomplete me tutorial=============================="
echo "https://medium.com/@rahul11061995/autocomplete-in-vim-for-js-developer-698c6275e341"
echo "==================================================================================="
echo  -e "\033[33;5m-----------------run $0 full for full install------------------------\033[0m"
