#!/bin/bash

sed -i 's/\/home\/metalx1000/$HOME/g' ~/.vimrc;grep "^iab" ~/.vimrc|awk '{print $2}'|sort > ~/.vim/skeleton/dic.lst

cp -v ~/.vimrc vimrc
cp -v ~/.tmux.conf tmux.conf
cp -v ~/.tern-config tern-config
cp -v ~/.vim/vim_templates.sh vim_templates.sh
cp -v ~/.vim/html_templates.sh html_templates.sh
cp -v ~/.vim/scripts_search.sh scripts_search.sh

cp -rv ~/.vim/skeleton/* skeleton/
cp -rv ~/.vim/scripts/* scripts/
#cp -rv ~/.vim/ftplugin/* ftplugin/

