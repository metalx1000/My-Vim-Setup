set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" This is the Vundle package, which can be found on GitHub.
" " For GitHub repos, you specify plugins using the
" " 'user/repository' format
" :PluginInstall to install
Plugin 'gmarik/vundle'
Plugin 'lukaszb/vim-web-indent'
Plugin 'valloric/YouCompleteMe'
"All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
"Set YouCompleteMe to a minimum of 3 char before active
let g:ycm_min_num_of_chars_for_completion = 3

"vimplugged
call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/seoul256.vim'
Plug 'junegunn/vim-peekaboo'
Plug 'junegunn/vim-slash'
Plug 'mattn/emmet-vim'
call plug#end()


"emmet
let g:user_emmet_mode='a'
let g:user_emmet_install_global = 0
"autocmd FileType php,html,css EmmetInstall
let g:user_emmet_leader_key='<C-Z>'


"seoul color setting
let g:seoul256_background = 233
colo seoul256

"this is my VIMRC file
syntax on
set shiftwidth=2 softtabstop=2 expandtab

"Highlight current line
hi CursorLine cterm=NONE ctermbg=darkgrey 
set cursorline

"set leader
let mapleader = ","


"I don't like auto indent
"set noautoindent
filetype indent on

"""""THIS IS COMMENTED OUT BECAUSE OF ERRORS""""
"""""High lighting tags"""""
"filetype plugin indent on

"au BufEnter,BufNew *.php :set filetype=html
au BufEnter,BufNew *.php :set syntax=php

command! Wq :wq
command! W :w
command! Q :q
:cmap Q! q!
:cmap q1 q!
"ab wq <ESC>dw:wq<CR>
"ab Wq <ESC>dw:wq<CR>

"fuzzy search for files
":find *.js
set path+=**
set wildmenu

"allow switching of buffers without saving first
":b <tab>
set hidden

""set theme to darkbackground
set background=dark

""""""Prevent indent on paste
function! WrapForTmux(s)
  if !exists('$TMUX')
    return a:s
  endif

  let tmux_start = "\<Esc>Ptmux;"
  let tmux_end = "\<Esc>\\"

  return tmux_start . substitute(a:s, "\<Esc>", "\<Esc>\<Esc>", 'g') . tmux_end
endfunction

let &t_SI .= WrapForTmux("\<Esc>[?2004h")
let &t_EI .= WrapForTmux("\<Esc>[?2004l")

function! XTermPasteBegin()
  set pastetoggle=<Esc>[201~
  set paste
  return ""
endfunction

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()
""""""END paste prevent indent

nnoremap <C-n> :bnext<CR>
nnoremap <C-p> :bprevious<CR>

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

"sets dictionary files for iab
set dictionary=~/.vim/skeleton/dic.lst

"use CTRL+N to auto compelte from dictionary
set dictionary+=/usr/share/dict/words
set complete+=.,w,b,u,t,i,kspell,k
set iskeyword+=-

"insert bootstrap templates
nnoremap <leader>bt :-1read $HOME/.vim/skeleton/bootstrap/index.html<CR>3j2f<i
nnoremap <leader>html :-1read $HOME/.vim/skeleton/html/index.html<CR>3j2f<i
nnoremap <leader>bash :-1read $HOME/.vim/skeleton/sh/bash_header.sh<CR>Gi
nnoremap <leader>node :-1read $HOME/.vim/skeleton/nodejs/header<CR>Gi
nnoremap <leader>letter :-1read !$HOME/.vim/skeleton/scripts/letter.sh<CR>G11k4li
nnoremap <leader>gcc :-1read $HOME/.vim/skeleton/gcc/chello.c<CR>Gi
nnoremap <leader>getlist :-1read $HOME/.vim/skeleton/getlist.js<CR>i
nnoremap <Leader>ve :e $MYVIMRC<CR>
nnoremap <Leader>vr :source $MYVIMRC<CR>

"c programming templates
iab gcchello <ESC>:-1read $HOME/.vim/skeleton/gcc/chello.c<CR>jjjjf/i
iab gccdir <ESC>:-1read $HOME/.vim/skeleton/gcc/gccdir.c<CR><F6>
iab gcccolor <ESC>:-1read $HOME/.vim/skeleton/gcc/gcccolor.c<CR><F6>
iab gccyn <ESC>:-1read $HOME/.vim/skeleton/gcc/gccyn.c<CR><F6>
iab gccinput <ESC>:-1read $HOME/.vim/skeleton/gcc/gccinput.c<CR><F6>
iab gccreadfile <ESC>:-1read $HOME/.vim/skeleton/gcc/readfile.c<CR><F6>
iab gccwritefile <ESC>:-1read $HOME/.vim/skeleton/gcc/writefile.c<CR><F6>
iab gccenterToContinue <ESC>:-1read $HOME/.vim/skeleton/gcc/gccentercontinue.c<CR><F6>

iab gpl3_header <ESC>:-1read $HOME/.vim/skeleton/licenses/gpl3.txt<CR><F6>

"phaser templates
iab pjsbackgroundcolor <ESC>:-1read $HOME/.vim/skeleton/phaser/backgroundcolor.js<CR><F6>
iab pjsgravity <ESC>:-1read $HOME/.vim/skeleton/phaser/gravity.js<CR><F6>
iab pjsfollowmouse <ESC>:-1read $HOME/.vim/skeleton/phaser/followMouse.js<CR><F6>
iab pjsmousedown <ESC>:-1read $HOME/.vim/skeleton/phaser/onmousedown.js<CR><F6>
iab pjskeys <ESC>:-1read $HOME/.vim/skeleton/phaser/keys.js<CR><F6>
iab pjsphysics <ESC>:-1read $HOME/.vim/skeleton/phaser/physics.js<CR>f-c
iab pjssprite <ESC>:-1read $HOME/.vim/skeleton/phaser/sprite.js<CR><F6>
iab pjsspriteAnimation <ESC>:-1read $HOME/.vim/skeleton/phaser/animation.js<CR><F6>
iab pjspaint1 <ESC>:-1read $HOME/.vim/skeleton/phaser/paint1.js<CR><F6>
iab pjsloadimage <ESC>^dwigame.load.image("", "res/img/"");<ESC>5F"p<DEL><ESC>2f"c
iab pjsfade <ESC>igame.add.tween().to( { alpha: [.1,1] }, 2000, "Linear", true);<ESC>2F(a
iab pjsclick <ESC>:-1read $HOME/.vim/skeleton/phaser/clickable.js<CR><F6>
iab pjscollide <ESC>:-1read $HOME/.vim/skeleton/phaser/collide.js<CR><F6>
iab pjscameraFollow <ESC>:-1read $HOME/.vim/skeleton/phaser/cameraFollow.js<CR><F6>
iab pjsloadMap <ESC>:-1read $HOME/.vim/skeleton/phaser/loadMap.js<CR><F6>
"iab phaclick <ESC>^dwpi.anchor.set(0.5);<CR><ESC>pi.inputEnabled = true;<CR><ESC>pi.events.onInputDown.add(, this);<ESC>F,i

"""ThreeJS templates
iab 3jscube <ESC>:-1read $HOME/.vim/skeleton/threejs/cube.js<CR>
iab 3jscubeTextured <ESC>:-1read $HOME/.vim/skeleton/threejs/cubeTextured.js<CR>
iab 3jssphere <ESC>:-1read $HOME/.vim/skeleton/threejs/sphere.js<CR>
iab 3jsplane <ESC>:-1read $HOME/.vim/skeleton/threejs/plane.js<CR>
iab 3jsmaterials <ESC>:-1read $HOME/.vim/skeleton/threejs/materials.js<CR>
iab 3jssprite <ESC>:-1read $HOME/.vim/skeleton/threejs/sprite.js<CR>
iab 3jscollision <ESC>:-1read $HOME/.vim/skeleton/threejs/collision.js<CR>
iab 3jsrenderer <ESC>:-1read $HOME/.vim/skeleton/threejs/renderCreate.js<CR>
iab 3jscamera <ESC>:-1read $HOME/.vim/skeleton/threejs/cameraCreate.js<CR>
iab 3jscone <ESC>:-1read $HOME/.vim/skeleton/threejs/cone.js<CR>
iab 3jsclick <ESC>:-1read $HOME/.vim/skeleton/threejs/clicked.js<CR>
iab 3jscameraRotate <ESC>:-1read $HOME/.vim/skeleton/threejs/cameraRotate.js<CR>
iab 3jscylinder <ESC>:-1read $HOME/.vim/skeleton/threejs/cylinder.js<CR>
iab 3jselected <ESC>:-1read $HOME/.vim/skeleton/threejs/allSelected.js<CR>
iab 3jsdae <ESC>:-1read $HOME/.vim/skeleton/threejs/loadDAE.js<CR>
iab 3jswindow_resize <ESC>:-1read $HOME/.vim/skeleton/threejs/windowresize.js<CR>
iab 3jsanimated_Sprites <ESC>:-1read $HOME/.vim/skeleton/threejs/animatedSprite.js<CR>

"Shell script templates
iab shsudo <ESC>:-1read $HOME/.vim/skeleton/asroot.sh<CR>
iab shselect <ESC>:-1read $HOME/.vim/skeleton/sh/select.sh<CR>
iab shcgi <ESC>:-1read $HOME/.vim/skeleton/cgi.sh<CR>
iab shcolor <ESC>:-1read $HOME/.vim/skeleton/shcolor.sh<CR>
iab sharray <ESC>:-1read $HOME/.vim/skeleton/sh/array.sh<CR>
iab sharrayrandom <ESC>:-1read $HOME/.vim/skeleton/sh/array_random.sh<CR>
iab shcheck_deps <ESC>:-1read $HOME/.vim/skeleton/sh/check_deps.sh<CR>
iab shgetopts <ESC>:-1read $HOME/.vim/skeleton/sh/getopts.sh<CR>
iab shifcontains <ESC>:-1read $HOME/.vim/skeleton/sh/ifcontains.sh<CR>

"Python Scritp Templates
iab pyhead <ESC>:-1read $HOME/.vim/skeleton/python/head.py<CR>
iab pydownloadText <ESC>:-1read $HOME/.vim/skeleton/python/downloadText.py<CR>
iab pydownloadFile <ESC>:-1read $HOME/.vim/skeleton/python/downloadFile.py<CR>

"CSS autocomplete Ctrl + x followed by  Ctrl + o
autocmd FileType css set omnifunc=csscomplete#CompleteCSS

"JavaScript Autocomplete Ctrl + x followed by  Ctrl + o
autocmd FileType js set omnifunc=javascriptcomplete#CompleteJS

"html Autocomplete Ctrl + x followed by  Ctrl + o
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags

autocmd FileType php set omnifunc=phpcomplete#CompletePHP
"auto close tags on space
"inoremap </ </<C-X><C-O>

set tags=$HOME/.vim/tags

"line numbering
set relativenumber
set number

"remove all trailing whitespace and fix indenting by pressing F6
nnoremap <F6> mp:let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>'pzzmqgg=G'qzz<cr>:let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>'pzz
nnoremap <Leader>cw mp:let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>'pzz
"nnoremap <F6> mq:set filetype=html<CR>gg=G'q
nnoremap <Leader>in mqgg=G'qzz
"nnoremap <F6> mqgg=G'qzz<cr>:let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>'pzz
nnoremap <F7> :call IndentPHPHTML()<cr>

:function IndentPHPHTML()
:  set ft=php
:  normal gg=G'qzz
:  set ft=html
:  normal gg=G'qzz
:  set ft=php
:  normal gg=G'qzz
:endfunction


"toggle set paste when F3 is clicked
set pastetoggle=<Leader>ps
set pastetoggle=<F3>

"start spell check
map <Leader>sc :setlocal spell! spelllang=en_us<CR>
map <F9> :setlocal spell! spelllang=en_us<CR>
map <F1> :read !$HOME/.vim/vim_templates.sh

map <F2> :'<,'> w! /tmp/vim_select.txt<cr>:'<,'>d<cr>:read !$HOME/.vim/scripts/html_select_options<CR>
"noremap <C-s> :'<,'>w! /tmp/vim_select.txt <CR>:read !$HOME/.vim/scripts/html_select_options<CR>
map <C-s> :'<,'> w! /tmp/vim_select.txt<cr>:'<,'>d<cr>:-1read !$HOME/.vim/scripts_search.sh<CR>

inoremap <C-t> <ESC>:read !$HOME/.vim/vim_templates.sh<CR>mqgg=G'qzzi
inoremap <C-h> <CR><ESC>:exe 'norm i' . system("$HOME/.vim/html_templates.sh")<CR>k0f<i

"uses system clipboard when yanking (center click)
"set clipboard=unnamed

"php templates
iab phpinclude <?php include(" ");?><ESC>F c
iab phpgetform <ESC>:-1read $HOME/.vim/skeleton/php/getform.php<CR>
iab phpredirect <ESC>:-1read $HOME/.vim/skeleton/php/redirect.php <CR>

"html templates
iab htmlbtn <ESC>:-1read $HOME/.vim/skeleton/html/button.html <CR>f<i
iab hbtn <ESC>:-1read $HOME/.vim/skeleton/html/button.html <CR>f<i
iab htmlinput <ESC>:-1read $HOME/.vim/skeleton/html/input.html <CR>2f"i
iab hinput <ESC>:-1read $HOME/.vim/skeleton/html/input.html <CR>2f"i
iab hflex <ESC>:-1read $HOME/.vim/skeleton/html/flex.html <CR>jI
iab htmlflex <ESC>:-1read $HOME/.vim/skeleton/html/flex.html <CR>jI
iab htmldrop <ESC>:-1read $HOME/.vim/skeleton/html/dropdown_select.html <CR>jI
iab hdrop <ESC>:-1read $HOME/.vim/skeleton/html/dropdown_select.html <CR>jI


"css templates
iab cssbtn <ESC>:-1read $HOME/.vim/skeleton/css/cssbutton_blue.css <CR>
iab cssbtnred <ESC>:-1read $HOME/.vim/skeleton/css/cssbutton_red.css <CR>
iab cssbtngreen <ESC>:-1read $HOME/.vim/skeleton/css/cssbutton_green.css <CR>

"bootstrap templates
iab btb <ESC>:-1read $HOME/.vim/skeleton/bootstrap/button.html<CR>k/><<CR>li
iab btbb <ESC>:-1read $HOME/.vim/skeleton/bootstrap/buttongroup.html<CR>jf>
iab btbcon <div class="container"> </div><ESC>F c
iab btnav <ESC>:-1read $HOME/.vim/skeleton/bootstrap/navbar.html<CR>/WebSite<CR>cit
iab btcard <ESC>:-1read $HOME/.vim/skeleton/bootstrap/cards.html<CR>/WebSite<CR>cit
iab btdropdownSearch <ESC>:-1read $HOME/.vim/skeleton/bootstrap/dropdownSearch.html<CR>
iab btnav_sticky <ESC>:-1read $HOME/.vim/skeleton/bootstrap/navbar_sticky.html<CR>/WebSite<CR>cit
iab btcheckbox <ESC>:-1read $HOME/.vim/skeleton/bootstrap/checkbox.html<CR>
iab btcheckboxcolors <ESC>:-1read $HOME/.vim/skeleton/bootstrap/checkboxs.html<CR>
iab btprogessbar <ESC>:-1read $HOME/.vim/skeleton/bootstrap/progressbar.html<CR>
iab btcollapse <ESC>:-1read $HOME/.vim/skeleton/bootstrap/collapsible.html<CR>
iab btinput <ESC>:-1read $HOME/.vim/skeleton/bootstrap/input.html<CR>
iab btpassword <ESC>:-1read $HOME/.vim/skeleton/bootstrap/password.html<CR>
iab btmedia <ESC>:-1read $HOME/.vim/skeleton/bootstrap/meida1.html<CR>
iab btfooter_sticky <ESC>:-1read $HOME/.vim/skeleton/bootstrap/footerSticky.html<CR>
iab btcarousel <ESC>:-1read $HOME/.vim/skeleton/bootstrap/carousel.html<CR>
iab btmedianested <ESC>:-1read $HOME/.vim/skeleton/bootstrap/meidanested.html<CR>
iab btmodal <ESC>:-1read $HOME/.vim/skeleton/bootstrap/modal.html<CR>
iab bttable <ESC>:-1read $HOME/.vim/skeleton/bootstrap/table.html<CR>
iab btimgcorner <ESC>:-1read $HOME/.vim/skeleton/bootstrap/imgcorner.html<CR>
iab btimgcircle <ESC>:-1read $HOME/.vim/skeleton/bootstrap/imgcircle.html<CR>
iab btthumbnail <ESC>:-1read $HOME/.vim/skeleton/bootstrap/thumbnail.html<CR>
iab btwell <ESC>:-1read $HOME/.vim/skeleton/bootstrap/well.html<CR>
iab btalert_success <ESC>:-1read $HOME/.vim/skeleton/bootstrap/alertsuccess.html<CR>
iab btalert_info <ESC>:-1read $HOME/.vim/skeleton/bootstrap/alertinfo.html<CR>
iab btalert_warning <ESC>:-1read $HOME/.vim/skeleton/bootstrap/alertwarning.html<CR>
iab btalert_danger <ESC>:-1read $HOME/.vim/skeleton/bootstrap/alertdanger.html<CR>
iab btlist <ESC>:-1read $HOME/.vim/skeleton/bootstrap/list.html<CR>
iab btimagegrid <ESC>:-1read $HOME/.vim/skeleton/bootstrap/imggrid.html<CR>

"HTML shortcuts
iab hh1 <h1> </h1><ESC>F c
iab hdiv <div id=" "></div><ESC>F c
iab ulist <ul id="list"><CR><li class="list_item"> </li><CR></ul><ESC>k2f c
iab htmlscript <script src=" "></script><ESC>F c

"download filter list.php
iab filterlist <ESC>:!wget "https://gitlab.com/metalx1000/html-javascript-filter-list/-/raw/master/list.php"<CR>i createList("listname",main,myarray,"function");<CR><?php include("list.php");?>

"jquery templates
"iab jqpost function(){<CR>var url = 'submit.php';<CR>$.post( url, {data:'test'}, function( data ) {<CR>console.log( data );<CR>});<CR>}
iab jqpost <ESC>:-1read $HOME/.vim/skeleton/jq/post.js<CR>
iab jqcl $(" ").click();<ESC>F c
iab jqlist <div class="container"><CR><h2>List Group With Linked Items</h2><CR><div class="list-group"><CR><a class="list-group-item">F</a><CR><a class="list-group-item">Second item</a><CR><a class="list-group-item">Third item</a><CR></div><CR></div><ESC>kkkkfFc
iab jqmodaltoggle $("#modal").modal('toggle');

"Javascript templates
iab jsfun function  (){<CR><CR>}<ESC>2k2f c
iab jssrc <script src=" "><script><ESC>F c
iab jskeydown <ESC>:-1read $HOME/.vim/skeleton/js/onkeydown.js<CR>
iab jskeyup <ESC>:-1read $HOME/.vim/skeleton/js/onkeyup.js<CR>
iab jsarrayloop <ESC>:-1read $HOME/.vim/skeleton/js/arrayforloop.js<CR>
iab jsrandomString <ESC>:-1read $HOME/.vim/skeleton/js/randomstring.js<CR>
iab jscheckboxes <ESC>:-1read $HOME/.vim/skeleton/js/checkbox.js<CR>
iab jsdropdownSearch <ESC>:-1read $HOME/.vim/skeleton/js/dropdownSearch.js<CR>
iab jsget <ESC>:-1read $HOME/.vim/skeleton/js/get.js<CR>
iab jspost <ESC>:-1read $HOME/.vim/skeleton/js/post.js<CR>
iab jsselectElement <ESC>:-1read $HOME/.vim/skeleton/js/select.js<CR>
iab jsexist <ESC>:-1read $HOME/.vim/skeleton/js/element_exists.js<CR>
iab byid document.getElementById(" ");<ESC>F c
iab byclass document.getElementsByClassName(" ");<ESC>F c
iab bytag document.getElementsByTagName(" ");<ESC>F c
iab urlhas if (window.location.href.indexOf("=") > -1) {}<ESC>F=c
iab rarray var randomItem = myArray[Math.floor(Math.random()*myArray.length)];
iab jsrandomArray var randomItem = myArray[Math.floor(Math.random()*myArray.length)];

iab Wq <ESC>i<DEL><ESC>:wq<CR>
iab wq <ESC>i<DEL><ESC>:wq<CR>


"force to use hjkl
"map <up> <nop>
"map <down> <nop>
"map <left> <nop>
"map <right> <nop>

"Copy and paste from system clipboard -- gvim might be needed
vnoremap <C-c> "+y
"vnoremap <C-c> "*y :let @+=@*<CR>
"nnoremap <C-S-v> "+P


"allow for Control Arrow keys in tmux
if &term =~ '^screen'
  " tmux will send xterm-style keys when its xterm-keys option is on
  execute "set <xUp>=\e[1;*A"
  execute "set <xDown>=\e[1;*B"
  execute "set <xRight>=\e[1;*C"
  execute "set <xLeft>=\e[1;*D"
endif

"fzf stuff
nnoremap <leader>f :Files .<CR>
nnoremap <leader>b :Buffers<CR>

"vim-slash stuff
noremap <plug>(slash-after) zz
if has('timers')
  "Blink 2 times with 50ms interval
  noremap <expr> <plug>(slash-after) slash#blink(2, 50)
endif

set encoding=utf-8

"Stop auto commenting of next line
"set formatoptions-=cro
au FileType * set fo-=c fo-=r fo-=o
