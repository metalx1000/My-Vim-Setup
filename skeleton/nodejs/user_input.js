const readline = require('readline');

const rl = readline.createInterface({ input: process.stdin, output: process.stdout });
const prompt = (query) => new Promise((resolve) => rl.question(query, resolve));


//usage inside aync function do not need closure demo only*
(async () => {
  const name = await prompt('Whats your Name: ')
  //can use name for next question if needed
  const lastName = await prompt(`Hello ${name} Whats your Last name?:` )
  //can prompt multiple times.
  console.log(name,lastName);
  rl.close()
})()

