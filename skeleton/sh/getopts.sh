function main(){
  options $@
}
 
function options(){
  local OPTIND opt i
  while getopts ":qcni:" opt; do
    case $opt in
      i) img="$OPTARG";;
      c) c=true;; #put link in clipboard
      q) qr=true;; # display qrcode (qrencode needed)
      n) n=true ;; # Send notification to desktop
      \?) help;exit 1 ;; # Handle error: unknown option or missing required argument.
    esac
  done
  shift $((OPTIND -1))
 
  if [ "$img" = "" ]
  then
    echo "No input file"
    exit 1
  fi
}

main $@
