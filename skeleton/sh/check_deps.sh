function check_depend(){
  #list for dependencies
  deps=( "/usr/bin/qrencode" "/usr/bin/xclip")
  packages=( qrencode xclip );

  #check for needed dependencies
  for i in "${deps[@]}"
  do
    if [ ! -f "$i" ];
    then
      echo "Attempting to install dependencies..."
      echo "Needed dependencies: ${packages[@]}"
      sudo apt-get install ${packages[@]} && break || (echo "Install of dependencies failed";exit 1) 
    fi
  done
}
