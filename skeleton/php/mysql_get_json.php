<?php
$server="localhost";
$username="<user>";
$password="<password>";
$database="<db>";
$table = "mytable";

// Create connection
$con=mysqli_connect("localhost","$username","$password","$database");

// Check connection
if (mysqli_connect_errno($con)){
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

//$ROOT = getenv('APP_ROOT_PATH');
//include("$ROOT/mysql/head.php");


$_GET = array_map('strip_tags', $_GET);
$_GET = array_map('htmlspecialchars', $_GET);

$result = mysqli_query($con,"SELECT * FROM $table");
$rows = array();
while($row = mysqli_fetch_array($result)) {
    $rows[] = $row;
}
print json_encode($rows);
mysqli_close($con);

?>

