//Sprite to Follow Mouse
//!!!add this function with sprite to UPDATE function!!!\\\
function followMouseDown(sprite){
  //check if sprite has pyhsics
  if ( sprite.body == null){
    console.log("Adding Physics to " + sprite.key);
    game.physics.enable(sprite, Phaser.Physics.ARCADE);
  }

  if ( sprite.speed === undefined ){
    var speed = 400;
  }else{
    var speed = sprite.speed;
  }

  //  only move when you click
  if (game.input.mousePointer.isDown)
  {
    //  400 is the speed it will move towards the mouse
    game.physics.arcade.moveToPointer(sprite, speed);

    //  if it's overlapping the mouse, don't move any more
    if (Phaser.Rectangle.contains(sprite.body, game.input.x, game.input.y))
    {
      sprite.body.velocity.setTo(0, 0);
    }
  }
  else
  {
    sprite.body.velocity.setTo(0, 0);
  }

}
