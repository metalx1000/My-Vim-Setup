//add this to preload function
//animated sprites
this.load.spritesheet('ani1', '/sprites/ani1.png', { frameWidth: 16, frameHeight: 31 });


//add this to create function
this.anims.create({
  key: 'key',
  frames: this.anims.generateFrameNumbers('ani1',{ start: 0, end: 2 }),
  frameRate: 3,
  repeat: -1
});

balloon = this.add.sprite(50, 300, 'balloon').setScale(4);
balloon.play('float');
balloon.on('animationcomplete', function(){this.destroy();}, this);

