const selectElement = document.querySelector('#element');

selectElement.addEventListener('change', (event) => {
  console.log(`You like ${event.target.value}`);
});
