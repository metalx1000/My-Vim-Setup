function create_data_list(items,name){
  var id = "list_" + Math.floor(Math.random() * 1000);
  var list = `<input list="${id}" name="${name}" id="${name}" autocomplete="off" autofocus>
  <datalist id="${id}">`;

  for(i of items){
    list += `<option value="${i}">`;
  }

  list += `</datalist>`;

  return list;
}
