function createCheckboxes(array){
  //Check if css is loaded and if not, then load
  if (!$("link[href='/checkbox.css]").length){
    $('<link href="css/checkbox.css" rel="stylesheet">').appendTo("head");
  }

  var boxes = "";
  for(var i = 0;i<array.length;i++){
    var item = array[i];
    //random id
    var id = Math.floor(Math.random()*100000000000000);
    boxes += `
    <div class="[ form-group ]">
    <input type="checkbox" name="${item}" id="${id}" autocomplete="off" />                                                              
    <div class="[ btn-group ]">
    <label for="${id}"  class="[ btn btn-primary ]">
    <span class="[ glyphicon glyphicon-ok ]"></span>
    <span> </span>
    </label>
    <label for="${id}" class="[ btn btn-default active ]">
    ${item}
    </label>
    </div>
    </div>
    `
  }

  return boxes;
}
