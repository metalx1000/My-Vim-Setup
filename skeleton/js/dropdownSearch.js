<!--put this in form to be submitted-->
<input type="text" id="item" name="item" value="" readonly>

function createDropdown(label,array){
  //Check if css is loaded and if not, then load
  if (!$("link[href='/dropdown.css]").length){
    $('<link href="css/dropdown.css" rel="stylesheet">').appendTo("head");
  }

  var dropdown = `
  <div class="dropdown">
  <button id="dropbtn" class="dropbtn" onclick="showDropdown()">${label}</button>
  <div id="myDropdown" class="dropdown-content">
  <input type="text" placeholder="Search.." id="myInput" onkeyup="filterFunction()">
  `;

  for(var i = 0;i<array.length;i++){
    var item = array[i];
    dropdown += `<a class="dropitem" href="#" onclick="itemSelect(this)">${item}</a>`;
  }

  dropdown += `
  </div>
  </div>`;

  return dropdown;
}

function itemSelect(e){
  $("#myDropdown").toggle();
  $("#item").val(e.innerHTML);
}

function showDropdown(){
  $("#myDropdown").toggle();
  $("#myInput").focus();
}

function filterFunction() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("myDropdown");
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}

