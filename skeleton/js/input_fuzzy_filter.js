/*
wget "https://raw.githubusercontent.com/mattyork/fuzzy/master/fuzzy-min.js"
*/
<script src="js/fuzzy-min.js"></script>
<script>
var list = [
  'Liam',
  'Noah',
  'Oliver',
  'William',
  'Elijah',
  'Adrian',
  'Christian',
  'Maverick',
  'Colton',
  'Elias',
  'Aaron',
  'Jose',
  'Jace',
  'Everett',
  'Declan',
  'Evan',
  'Kayden',
  'Amir',
  'Richard',
  'Alejandro',
  'Steven',
  'Jesse',
  'Dawson',
  'Bryce',
  'Avery',
  'Oscar',
  'Patrick',
  'Archer',
  'Barrett',
  'Leon',
  'Colt',
  'Charlie',
  'Peter',
  'Kaleb',
  'Lukas',
  'Beckett',
  'Jeremy',
  'Preston',
  'Enzo',
  'Luka',
  'Andres',
  'Marcus',
  'Felix',
  'Mark',
  'Ace',
  'Brantley',
  'Atlas',
  'Remington',
  'Maximus',
  'Matias',
  'Walker'
];

function search(q){
  var results = fuzzy.filter(q, list);
  console.log(results);
}
</script>
