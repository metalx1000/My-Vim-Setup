function loadSprite(img){
  //load sprite
  var texture = new THREE.TextureLoader().load( img );
  var material = new THREE.SpriteMaterial( { map: texture } );
  //  var material = new THREE.SpriteMaterial( { map: texture, color: 0xff0000 } );                                                                           
  var sprite = new THREE.Sprite( material );
  sprite.position.set( 0, 0, 0 );
  sprite.scale.set( 1, 1, 1.0 ); // imageWidth, imageHeight
  scene.add( sprite );

  return sprite;
}

