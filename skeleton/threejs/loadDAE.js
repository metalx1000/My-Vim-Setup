//Load DAE Scene
//wget "https://threejs.org/examples/js/loaders/ColladaLoader.js" -O libs/ColladaLoader.js
//<script src="libs/ColladaLoader.js"></script>
function loadDAE(dae){
  var model;
  var loadingManager = new THREE.LoadingManager( function () {
    scene.add( model );
  } );
  // collada
  var loader = new THREE.ColladaLoader( loadingManager );
  model = loader.load( dae, function ( collada ) {
    model = collada.scene;
  } );

}

