//add to main script
var mouse = new THREE.Vector2(), INTERSECTED;
var clickable = [];

//add to init
raycaster = new THREE.Raycaster();
document.addEventListener( 'mousemove', onDocumentMouseMove, false );

//add to update function
updateMouseOver();

function onDocumentMouseMove( event ) {
  event.preventDefault();
  mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
  mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
}

function updateMouseOver(){
  raycaster.setFromCamera( mouse, camera );
  var intersects = raycaster.intersectObjects( clickable );
  console.log(intersects.length);
  if ( intersects.length > 0 ) {
    console.log(intersects[0]);
    intersects[0].object.rotation.x+=.1;
  }

}
