function createSphere(size){
  //SphereGeometry(radius : Float, 
  //   widthSegments : Integer, 
  //   heightSegments : Integer, 
  //   phiStart : Float, 
  //   phiLength : Float, 
  //   thetaStart : Float, 
  //   thetaLength : Float)

  var material = new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff } ;)
  var geometry = new THREE.SphereBufferGeometry( size / 2, 32, 16 );
  var mesh = new THREE.Mesh( geometry, material );
  scene.add(mesh);

  return mesh;
}
