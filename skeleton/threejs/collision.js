function collision_check(obj1,obj2){
  var obj1 = new THREE.Box3().setFromObject(obj1);
  var obj2 = new THREE.Box3().setFromObject(obj2);
  return obj1.intersectsBox(obj2);
}
