function createLights(){
  //set to true to view light positions
  var helpers = false;

  light1 = new THREE.DirectionalLight( 0xffffff, 1 );
  light1.position.set( 5, 5, 10 ).normalize();
  scene.add( light1 );

  //Add helper to view light position
  if(helpers){
    light1.helper = new THREE.DirectionalLightHelper( light1, 50 );
    scene.add( light1.helper);
  }

  light2 = new THREE.DirectionalLight( 0xffffff, 1 );
  light2.position.set(-1,-1,-5).normalize();
  scene.add( light2 );

  //Add helper to view light position
  if(helpers){
    light2.helper = new THREE.DirectionalLightHelper( light2, 50 );
    scene.add( light2.helper);
  }

  return true;
}

