var animations = []; // animators

function createSprite(img){
  // ANIMATION!
  var Texture = new THREE.ImageUtils.loadTexture( img );
  animations.push(new TextureAnimator( Texture, 6, 1, 6, 175 )); // texture, #horiz, #vert, #total, duration.
  var Material = new THREE.MeshBasicMaterial( { map: Texture, side:THREE.DoubleSide, transparent : true  } );
  var Geometry = new THREE.PlaneGeometry(60, 80, 1, 1);
  var sprite = new THREE.Mesh(Geometry, Material);
  scene.add(sprite);
  return sprite;
}

//Put in update Function
animations.forEach(function(a){
  var delta = clock.getDelta();
  a.update(1000 * delta);
});

