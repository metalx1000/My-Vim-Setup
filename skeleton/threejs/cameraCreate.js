function createCamera(){
  camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 5000 );
  camera.position.z = 4;
  camera.position.y = 4;
  camera.lookAt(scene.position);

  //addcontrols
  controls = new THREE.OrbitControls( camera, renderer.domElement );
}

