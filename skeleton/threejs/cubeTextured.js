function createCubeTextured(img){
  var size = 1;
  var texture = new THREE.TextureLoader().load( img );
  var geometry = new THREE.BoxBufferGeometry( size, size, size );
  var material = new THREE.MeshBasicMaterial( { map: texture } );
  mesh = new THREE.Mesh( geometry, material );
  scene.add( mesh );

  return mesh;
}
