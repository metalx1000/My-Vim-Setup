function createCone(){
  //ConeGeometry(radius : Float, height : Float, radialSegments : Integer,
  //  heightSegments : Integer, openEnded : Boolean, thetaStart : Float, thetaLength : Float)
  var geometry = new THREE.ConeGeometry( 5, 20, 32 );
  var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
  var cone = new THREE.Mesh( geometry, material );
  scene.add( cone );

  return cone;
}
