function createCylinder(){
  //CylinderGeometry(radiusTop : Float, radiusBottom : Float, height : Float,
  //radialSegments : Integer, heightSegments : Integer,
  //openEnded : Boolean, thetaStart : Float, thetaLength : Float)
  var geometry = new THREE.CylinderGeometry( 5, 5, 20, 32 );
  var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
  var cylinder = new THREE.Mesh( geometry, material );
  scene.add( cylinder );

  return cylinder;
}
