//check for window resize
window.addEventListener( 'resize', onWindowResize, false );

function onWindowResize() {
  var WIDTH = window.innerWidth,
  HEIGHT = window.innerHeight;
  renderer.setSize(WIDTH, HEIGHT);
  camera.aspect = WIDTH / HEIGHT;
  camera.updateProjectionMatrix();
}

