function createPlane(){
  //PlaneGeometry(width : Float, height : Float, widthSegments : Integer, heightSegments : Integer)
  var geometry = new THREE.PlaneGeometry( 5, 20, 32 );
  var material = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide} );
  var plane = new THREE.Mesh( geometry, material );
  scene.add( plane );

  return plane;
}
